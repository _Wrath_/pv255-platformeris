// This makes the character turn to face the current movement speed per default.
var autoRotate : boolean = true;
var maxRotationSpeed : float = 360;

private var motor : CharacterMotor;
private var anim : Animator;
private var col : CapsuleCollider;
private var ceilingCheck : Transform ;								// A position marking where to check for ceilings

// Use this for initialization
function Awake () {
	motor = GetComponent(CharacterMotor);
	anim = GetComponent(Animator);
	col = GetComponent(CapsuleCollider);
	anim.SetBool ("Grounded", true);	
	anim.SetBool ("Crouch", false);
	
}

// Update is called once per frame
function Update () {
	// Get the input vector from keyboard or analog stick
	var directionVector = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);
	
	
	
	if (directionVector != Vector3.zero) {
	
		if (Input.GetKey (KeyCode.LeftArrow) || Input.GetKey (KeyCode.RightArrow) || Input.GetKey (KeyCode.D) || Input.GetKey (KeyCode.A)) {
				anim.SetFloat ("Speed", 0.05f);
				motor.movement.maxForwardSpeed = 8;
				motor.movement.maxBackwardsSpeed = 8;
				motor.movement.maxSidewaysSpeed = 8;
				if (Input.GetKey (KeyCode.LeftControl)) {
					anim.SetFloat("Speed",0.5f);
					motor.movement.maxForwardSpeed = 8;
					motor.movement.maxBackwardsSpeed = 8;
					motor.movement.maxSidewaysSpeed = 8;
				}
			} 
			else {
				anim.SetFloat("Speed",0f);
			}
			if (Input.GetKey (KeyCode.Space)) {
					
				col.height =  0.007;
			}
			else {
				
				col.height =  0.017;
			}
			
			if (Input.GetKey (KeyCode.LeftShift)) {
				anim.SetBool ("Crouch", true);
				motor.movement.maxForwardSpeed = 2;
				motor.movement.maxBackwardsSpeed = 2;
				motor.movement.maxSidewaysSpeed = 2;
			}
			else 
			{
				anim.SetBool ("Crouch", false);
			}
		// Get the length of the directon vector and then normalize it
		// Dividing by the length is cheaper than normalizing when we already have the length anyway
		var directionLength = directionVector.magnitude;
		directionVector = directionVector / directionLength;
		
		// Make sure the length is no bigger than 1
		directionLength = Mathf.Min(1, directionLength);
		
		// Make the input vector more sensitive towards the extremes and less sensitive in the middle
		// This makes it easier to control slow speeds when using analog sticks
		directionLength = directionLength * directionLength;
		
		// Multiply the normalized direction vector by the modified length
		directionVector = directionVector * directionLength;
	}
	
	// Rotate the input vector into camera space so up is camera's up and right is camera's right
	//directionVector = Camera.main.transform.rotation * directionVector;
	
	// Rotate input vector to be perpendicular to character's up vector
	//var camToCharacterSpace = Quaternion.FromToRotation(-Camera.main.transform.forward, transform.up);
	//directionVector = (camToCharacterSpace * directionVector);
	
	// Apply the direction to the CharacterMotor
	motor.inputMoveDirection = directionVector;
	motor.inputJump = Input.GetButton("Jump");
	
	// Set rotation to the move direction	
	if (autoRotate && directionVector.sqrMagnitude > 0.01) {
		var newForward : Vector3 = ConstantSlerp(
			transform.forward,
			directionVector,
			maxRotationSpeed * Time.deltaTime		
		);
		newForward = ProjectOntoPlane(newForward, transform.up);
		transform.rotation = Quaternion.LookRotation(newForward, transform.up);
	}
}

function ProjectOntoPlane (v : Vector3, normal : Vector3) {
	return v - Vector3.Project(v, normal);
}

function ConstantSlerp (from : Vector3, to : Vector3, angle : float) {
	var value : float = Mathf.Min(1, angle / Vector3.Angle(from, to));
	return Vector3.Slerp(from, to, value);
}

// Require a character controller to be attached to the same game object
@script RequireComponent (CharacterMotor)
@script AddComponentMenu ("Character/Platform Input Controller")
