﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour 
{

    int playerHealth = 100;
    int maxPlayerHealth = 100;

    public void Awake()
    {
        // do not destroy this game object during loading new scene
        DontDestroyOnLoad(gameObject);
    }

	public void changeHealth(int delta)
    {
        playerHealth += delta; 
        
        if(playerHealth > maxPlayerHealth)
        {
            playerHealth = maxPlayerHealth;
        }

        if (playerHealth < 0)
        {
            playerHealth = 0;
        }

        print("Health: " + playerHealth);
    }

    public int getHealth()
    {
        return playerHealth;
    }

    public int getMaxHealth()
    {
        return maxPlayerHealth;
    }
	
    public float getHealthPercent()
    {
        float hpercent = (float)playerHealth / (float)maxPlayerHealth;
        return hpercent;
    }

    public void loadLevel(string levelName)
    {
        //TODO add fancy effect here, such as fading...
        Application.LoadLevel(levelName);
    }

}
