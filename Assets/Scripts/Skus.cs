﻿using UnityEngine;
using System.Collections;

public class Skus : MonoBehaviour {

	public bool isQuit = false;
	public AudioClip onMouseClip;

	void OnMouseEnter(){
		renderer.material.color = Color.red;
		renderer.transform.localScale = new Vector2(renderer.transform.localScale.x +0.1f,renderer.transform.localScale.y +0.1f);
		audio.PlayOneShot (onMouseClip);
	}

	void OnMouseExit(){
		renderer.material.color = Color.white;
		renderer.transform.localScale = new Vector2(renderer.transform.localScale.x - 0.1f,renderer.transform.localScale.y - 0.1f); 
	}

	void OnMouseUp(){
		if (isQuit==true) {
			Application.LoadLevel("MainMenu");
		}
		else {
			Application.LoadLevel(1);
		}
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.Escape)) {
			Application.Quit();
		}
	}
}
