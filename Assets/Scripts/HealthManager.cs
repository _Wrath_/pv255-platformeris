﻿using UnityEngine;
using System.Collections;

public class HealthManager : MonoBehaviour {

	public float currentHealth { get; set;}
	public float maxHealth = 100;
	public GUIText text;

	// Use this for initialization
	void Start () {
		//DontDestroyOnLoad ();
		text.text = "100";
		currentHealth = 100;
	}

	public float barDisplay; //current progress
	public Vector2 pos = new Vector2(20,40);
	public Vector2 size = new Vector2(413, 31);
	public Texture2D emptyTex;
	public Texture2D fullTex;
	public GUIStyle progress_empty, progress_full;
	
	void OnGUI() {
		//draw the background:
		GUI.BeginGroup(new Rect(pos.x, pos.y, size.x, size.y));
			//GUI.Box(new Rect(0,0, size.x, size.y), emptyTex);
			GUI.Box(new Rect(0,0, size.x, size.y), emptyTex, progress_empty);

				//draw the filled-in part:
				GUI.BeginGroup(new Rect(0,0, barDisplay, size.y));
					//GUI.Box(new Rect(0,0, size.x, size.y), fullTex);
					GUI.Box(new Rect(0,0, size.x, size.y), fullTex, progress_full);
				GUI.EndGroup();
		GUI.EndGroup();
	}
	
	void Update() {
		currentHealth = currentHealth - 0.01f;
		barDisplay = (currentHealth * 413) / 100 ;
		text.text = Mathf.RoundToInt(currentHealth).ToString ();
		checkMaxHealth (currentHealth);
	}

    void checkMaxHealth(float current)
	{
		if (current > maxHealth) {
			current = maxHealth;		
		}
	}


}
