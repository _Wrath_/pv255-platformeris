﻿using UnityEngine;
using System.Collections;

public class HideScript : MonoBehaviour {
	public Animator anim;
	public CapsuleCollider cc;
	public TextMesh textmesh;

	bool ishidden = false;

	// Use this for initialization
	void Awake () {
		anim.SetBool ("Hide", false);
		textmesh.renderer.enabled = false;

		textanim.SetBool ("play", false);
	}

    void OnTriggerEnter()
	{
		textmesh.renderer.enabled = true;
	}

	void OnTriggerExit()
	{	
		textmesh.text  = "Press H to hide";
		textmesh.renderer.enabled = false;
	}

	void OnTriggerStay()
	{
		
		textmesh.fontStyle = FontStyle.Normal;
		textmesh.renderer.enabled = true;
		textmesh.text = "Press H to hide";
		textanim.SetBool ("play", true);
		if (Input.GetKey(KeyCode.H)) 
		{
			anim.SetBool ("Hide", true);
			cc.enabled = false;
			ishidden = true;
		}
	}

	void Update()
	{
		if (ishidden == true) {
			//int index = Mathf.RoundToInt((Time.time * framesPerSecond) % frames.Length);
			//renderer.material.mainTexture = frames[index];

			textmesh.fontStyle = FontStyle.BoldAndItalic;
			textmesh.renderer.material.color = Color.red;
			textmesh.text  = "U are hidden";
			if(Input.GetKey(KeyCode.RightArrow) 
			   || Input.GetKey(KeyCode.LeftArrow) 
			   || Input.GetKey(KeyCode.Space) 
			   || Input.GetKey(KeyCode.A)
			   || Input.GetKey(KeyCode.D))
			{
				ishidden = false;
				anim.SetBool ("Hide", false);
				textmesh.renderer.material.color = Color.cyan;
			}
		}
	}

	public Animator textanim;
	/*function Update() {
		int index = (Time.time * framesPerSecond) % frames.Length;
		renderer.material.mainTexture = frames[index];
	}*/
}
