﻿using UnityEngine;
using System.Collections;

public class TakingInjection : MonoBehaviour {

	public AudioClip takingInjectionClip;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider col){
		if (col.gameObject.name == "injection") {
			audio.PlayOneShot (takingInjectionClip);			}
		}
}
