﻿#pragma strict

var isQuit=false;

function OnMouseEnter(){
//change text color
renderer.material.color=Color.red;
renderer.transform.localScale.x = renderer.transform.localScale.x +0.1; 
renderer.transform.localScale.y = renderer.transform.localScale.y +0.1; 
}

function OnMouseExit(){
//change text color
renderer.material.color = Color.white;
renderer.transform.localScale.x = renderer.transform.localScale.x -0.1; 
renderer.transform.localScale.y = renderer.transform.localScale.y -0.1; 
}

function OnMouseUp(){
//is this quit
if (isQuit==true) {
//quit the game
Application.Quit();
}
else {
//load level
Application.LoadLevel(1);
}
}

function Update(){
//quit game if escape key is pressed
if (Input.GetKey(KeyCode.Escape)) { Application.Quit();
}
}