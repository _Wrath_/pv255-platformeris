# _NonVersioned Assets folder

* Content of this folder (except this README.md file) is ignored by GIT.

* In this folder, you can (should) place all your assetts, that are not intended to be versioned (e. g. temporary models).

